
# coding: utf-8

# In[1]:

import pandas as pd
import numpy as np
import cPickle as pickle

from sklearn.ensemble import GradientBoostingRegressor
from sklearn.grid_search import GridSearchCV
import sklearn.preprocessing as pre
from sklearn.metrics import fbeta_score, make_scorer
from ggplot import *
import metric

# In[20]:

train_data=pd.read_csv('train.csv')
#test_data=pd.read_csv('test.csv')
submission=pd.read_csv('sampleSubmission.csv')

'''
def drop_catData(df):
    df= df.drop(['var1','var2','var3','var4','var5','var6','var7','var8','var9','dummy'],axis=1)
    return df

num_data=drop_catData(train_data)
num_data=num_data.fillna(num_data.median()) # fill nans in the data
#tnum_data=drop_catData(test_data)
'''

#PROCESS TRAIN DATA
def conv_to_ordinal(df):
    le = pre.LabelEncoder()
    for col in df.columns:
        if col !='var7':
            if df[col].dtype=='O':
                le.fit(df[col])
                df[col]=le.transform(df[col])
    return df
train_data=conv_to_ordinal(train_data)
train_data['var7'] = pd.DataFrame(data={'var7': np.unique(train_data.var7,
                      return_inverse=True)[1]})
train_data=train_data.fillna(train_data.mean())  # mean puts score 0.34 on test
labels=np.array(train_data['target'])

#Randomly select portion of the data
#rows = np.random.choice(train_data.index.values, 200000)
#train = train_data.ix[rows]
#num_data=num_data.set_index('id')


  


# In[13]:

#train on randomly sampled data 
#train=np.round(train, 3)
labels=np.array(train_data['target'])

gini_weighted = make_scorer(metric.normalized_weighted_gini, greater_is_better=True)

##### GRID SEARCH for graident boossting regressor

# In[ ]:

param_grid = {'learning_rate': [0.1],
              'max_depth': [4,6],
              'min_samples_leaf': [5,7],  # depends on the nr of training examples
              'max_features': [1,0.5], ## not possible in our example (only 1 fx)
	      'min_samples_split': [2,5],
	      'loss':['lad']
              }

est = GradientBoostingRegressor()
# this may take some minutes
gs_cv = GridSearchCV(est, param_grid, n_jobs=10,verbose=2, scoring=gini_weighted).fit(np.array(train_data.drop(['target','id'],axis=1)), labels)

# best hyperparameter setting
print('Best hyperparameters: %r' % gs_cv.best_params_)

pickle.dump(gs_cv,open('grid_search','w+'))          
